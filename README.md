<h1 align="center">Hi 👋</h1>
<h3 align="center">A thinker sees his own actions as experiments and questions--as attempts to find out something -- Friedrich Nietzsche</h3>

- 🔭 I’m currently working on NepPreter
- 🌱 I’m currently learning **Rust**
- 📝 I regularly write articles on [voidash.github.io/blog/](https://voidash.github.io/blog/)


<h2 align="left"> Blog Posts</h2>
 
<!-- BLOG-POST-LIST:START -->
- [Introducing Nushell](https://voidash.github.io/blog/posts/2022/introducing-nushell/)
- [Uninstall System Apps in Android With ADB](https://voidash.github.io/blog/posts/2022/uninstall-system-apps-in-android-with-adb/)
- [How to switch between Nepali and English keyboard layouts in Linux](https://voidash.github.io/blog/posts/2022/switching-keyboard-layouts-in-linux/)
- [Scripting with Rust: A basic Guide](https://voidash.github.io/blog/posts/2021/rust-as-script/)
- [Rust New Competitor to C and C++](https://voidash.github.io/blog/posts/2021/rust-new-competitor-to-c-and-c++/)
<!-- BLOG-POST-LIST:END -->



<p align="center">
  <a target= "_blank" href="mailto:ashish.thapa477@gmail.com" alt="Mail"><img height='45' src="./icons/email.png"></a>
  <a target= "_blank" href="https://www.facebook.com/voidash" alt="Facebook"><img height='45' src="./icons/facebook.png"></a>
  <a target= "_blank" href="https://twitter.com/voidash_" alt="Twitter"><img height='45' src="./icons/twitter.png"></a>
</p>



